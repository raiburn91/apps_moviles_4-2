package com.example.recicle_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.recicle_view.ListData;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListData[] listData = new ListData[]{
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Malboro","cigarros",0,R.drawable.cigarros),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Botella de agua","Embotellados",0,R.drawable.agua),
                new ListData("Manzana","Frutas",50,R.drawable.apple),
                new ListData("Galleta","Reposteria",100,R.drawable.galleta),
                new ListData("Malboro","cigarros",0,R.drawable.cigarros),
                new ListData("Maruchan","sopa",150,R.drawable.maruchan),
                new ListData("Agua","Embotellados",0,R.drawable.agua),
                new ListData("Malboro","cigarros",0,R.drawable.cigarros)
        };
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recView);

        ListAdapter listAdapter = new ListAdapter(listData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(listAdapter);
    }
}
